package controllers

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/helpers"
	"diluan/services"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type ComboController interface {
	All(ctx *gin.Context)
	Create(ctx *gin.Context)
	Update(ctx *gin.Context)
	FindById(ctx *gin.Context)
	Delete(ctx *gin.Context)
	CreateProduct(ctx *gin.Context)
}

type Product struct {
	ID       uint64 `json:"id"`
	Quantity uint64 `json:"quantity"`
}

type comboController struct {
	comboService        services.ComboService
	jwtService          services.JWTService
	productService      services.ProductService
	comboProductService services.ComboProductService
}

func NewComboController(comboService services.ComboService, jwtService services.JWTService, productService services.ProductService, comboProductService services.ComboProductService) ComboController {
	return &comboController{
		comboService:        comboService,
		jwtService:          jwtService,
		productService:      productService,
		comboProductService: comboProductService,
	}
}

func (controller *comboController) All(ctx *gin.Context) {
	var combo []entities.Combo = controller.comboService.All()
	res := helpers.BuildResponse(true, "Oke", combo)
	ctx.JSON(http.StatusOK, res)
}

func (controller *comboController) Create(ctx *gin.Context) {
	var comboCreateDTO dto.ComboCreateDTO
	var Products []Product

	errDto := ctx.ShouldBind(&comboCreateDTO)
	if errDto != nil {
		response := helpers.BuildErrorResponse("Failed to process request", errDto.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	var line_items = []byte(comboCreateDTO.LineItems)
	json.Unmarshal(line_items, &Products)

	product_ids := convertProductIds(Products)
	if len(product_ids) != len(Products) || len(line_items) == 0 {
		response := helpers.BuildErrorResponse("Not Found Product", "Please Choose  Product", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	image_url := helpers.Upload(ctx)
	if !image_url.Status {
		response := helpers.BuildErrorResponse("Failed to process request", image_url.Message, helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	idChanel := make(chan string)
	go getUserIDByToken(ctx, idChanel)
	user_id := <-idChanel
	id, err := strconv.ParseUint(user_id, 10, 64)
	if err != nil {
		response := helpers.BuildErrorResponse("You dont have permission", "You are not role", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	totalPrice := 0
	productByIDs := controller.productService.FindByIds(product_ids)
	for _, product := range productByIDs {
		totalPrice += int(product.Price) * int(getQuantityProducts(product.ID, Products))
	}

	comboCreateDTO.CreatedBy = id
	comboCreateDTO.UpdatedBy = 0
	comboCreateDTO.ImageUrl = image_url.Message
	comboCreateDTO.TotalPrice = uint64(totalPrice)
	fmt.Println(comboCreateDTO)
	result := controller.comboService.Create(comboCreateDTO)
	if idNul := result.ID; idNul == 0 {
		response := helpers.BuildErrorResponse("Combe exited", "Combe exited", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	for _, product := range productByIDs {
		productDTO := dto.ComboProductCreateDto{
			Name:      product.Name,
			ImageUrl:  product.ImageUrl,
			ComboID:   result.ID,
			Status:    true,
			Price:     product.Price,
			ProductID: product.ID,
			Quantity:  getQuantityProducts(product.ID, Products),
			Unit:      product.Unit,
		}
		fmt.Println(productDTO)
		controller.comboProductService.Create(productDTO)
	}
	res := helpers.BuildResponse(true, "oke", result)
	ctx.JSON(http.StatusCreated, res)

}

func (controller *comboController) Update(ctx *gin.Context) {
	var updateComboDto dto.ComboUpdateDTO
	errDto := ctx.ShouldBind(&updateComboDto)
	if errDto != nil {
		response := helpers.BuildErrorResponse("Failed to process request", errDto.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	idParam, _ := strconv.ParseInt(ctx.Param("id"), 0, 0)

	if !controller.comboService.FindByNameUpdate(int64(updateComboDto.ID), updateComboDto.Name) {
		response := helpers.BuildErrorResponse("Failed to process request", "Duplicate Name", helpers.EmptyObj{})
		ctx.JSON(http.StatusConflict, response)
		return
	}

	combo := controller.comboService.FindById(uint64(idParam))
	if combo.ID == 0 {
		response := helpers.BuildErrorResponse("Failed to process request", "Combo Not Found", helpers.EmptyObj{})
		ctx.JSON(http.StatusConflict, response)
		return
	}
	idChanel := make(chan string)
	go getUserIDByToken(ctx, idChanel)
	user_id := <-idChanel
	id, err := strconv.ParseUint(user_id, 10, 64)

	if err != nil {
		response := helpers.BuildErrorResponse("You dont have permission", "You are not role", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	_, errFile := ctx.FormFile("image_url")
	if errFile == nil {
		image_url := helpers.Upload(ctx)
		updateComboDto.ImageUrl = image_url.Message
	}

	updateComboDto.UpdatedBy = id
	result := controller.comboService.Update(updateComboDto)
	if updateComboDto.Status == false {
		controller.comboService.UpdateStatus(updateComboDto)
	}
	res := helpers.BuildResponse(true, "oke", result)
	ctx.JSON(http.StatusCreated, res)
}

func (controller *comboController) FindById(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 0, 0)

	if err != nil {
		response := helpers.BuildErrorResponse("Not Found Data", "Not Found Data", helpers.EmptyObj{})
		ctx.JSON(http.StatusNotFound, response)
		return
	}
	combo := controller.comboService.FindById(uint64(id))
	if idNull := combo.ID; idNull == 0 {
		response := helpers.BuildErrorResponse("Not Found Data", "Not Found Data", helpers.EmptyObj{})
		ctx.JSON(http.StatusNotFound, response)
		return
	}
	res := helpers.BuildResponse(true, "oke", combo)
	ctx.JSON(http.StatusOK, res)
}

func (controller *comboController) Delete(ctx *gin.Context) {
	var combo entities.Combo
	id, err := strconv.ParseInt(ctx.Param("id"), 0, 0)

	if err != nil {
		response := helpers.BuildErrorResponse("Not Found Data", "Not Found Data", helpers.EmptyObj{})
		ctx.JSON(http.StatusNotFound, response)
		return
	}
	comboCheck := controller.comboService.FindById(uint64(id))
	if idNull := comboCheck.ID; idNull == 0 {
		response := helpers.BuildErrorResponse("Not Found Data", "Not Found Data", helpers.EmptyObj{})
		ctx.JSON(http.StatusNotFound, response)
		return
	}
	combo.ID = uint64(id)
	controller.comboService.Delete(combo)
	res := helpers.BuildResponse(true, "oke", "Delete Success")
	ctx.JSON(http.StatusOK, res)

}

func (controller *comboController) CreateProduct(ctx *gin.Context) {
	var ComboProductCreateDto dto.ComboProductCreateDto
	var ComboUpdateDTO dto.ComboUpdateDTO

	errDTO := ctx.ShouldBind(&ComboProductCreateDto)
	if errDTO != nil {
		res := helpers.BuildErrorResponse("Failed to process request ", errDTO.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	idParam, _ := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if idParam != ComboProductCreateDto.ComboID {
		res := helpers.BuildErrorResponse("Combo ID does not match", "Combo ID does not match", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	product := controller.productService.FindById(ComboProductCreateDto.ProductID)
	if product.ID == 0 {
		res := helpers.BuildErrorResponse("Product Not Found", "Product Not Found", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	comboProduct := controller.comboProductService.FindByProductAndComboID(product.ID , ComboProductCreateDto.ComboID)
	
	if comboProduct.ID != 0 {
		res := helpers.BuildErrorResponse("Product exited combo", "Product exited combo", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	combo := controller.comboService.FindById(ComboProductCreateDto.ComboID)
	if combo.ID == 0 {
		res := helpers.BuildErrorResponse("Combo Not Found", "Combo Not Found", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	totalPrice := combo.TotalPrice

	ComboProductCreateDto.Name = product.Name
	ComboProductCreateDto.ImageUrl = product.ImageUrl
	ComboProductCreateDto.Price = product.Price
	ComboProductCreateDto.Unit = product.Unit
	ComboProductCreateDto.Status = true
	comboProduct = controller.comboProductService.Create(ComboProductCreateDto)

	totalPrice  += ComboProductCreateDto.Quantity * product.Price
	ComboUpdateDTO.TotalPrice = totalPrice
	ComboUpdateDTO.ID = combo.ID
	controller.comboService.Update(ComboUpdateDTO)
	
	res :=helpers.BuildResponse(true , "Oke" , comboProduct)
	ctx.JSON(http.StatusCreated , res)

}

func getUserIDByToken(ctx *gin.Context, idChannel chan string) string {
	token := ctx.GetHeader("Authorization")
	aToken, err := services.NewJWTService().ValidateToken(token)
	if err != nil {
		panic(err.Error())
	}
	claims := aToken.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	idChannel <- id
	return id
}

func convertProductIds(Products []Product) []string {
	var ids []string
	for _, value := range Products {
		ids = append(ids, strconv.Itoa(int(value.ID)))
	}
	return ids
}

func isIntoComboProduct(comboProduct []entities.ComboProduct, ID int64) bool {
	var check = false
	for _, cbProduct := range comboProduct {
		if cbProduct.ProductID == uint64(ID) {
			check = true
		}
	}
	return check
}

func getIdAndQuantityForm(Products []Product, ID uint64) Product {
	for _, product := range Products {
		if product.ID == ID {
			return product
		}
	}
	return Product{}
}

func getQuantityProducts(id uint64, Products []Product) uint64 {
	quantity := uint64(0)
	for _, value := range Products {
		if value.ID == id {
			return value.Quantity
		}
	}
	return quantity
}

func getQuantityByProductDB(id uint64, comboProduct []entities.ComboProduct) uint64 {
	quantity := uint64(0)
	for _, value := range comboProduct {
		if value.ProductID == id {
			return value.Quantity
		}
	}
	return quantity
}

func difference(slice1 []string, slice2 []string) []string {
	diffStr := []string{}
	m := map[string]int{}

	for _, s1Val := range slice1 {
		m[s1Val] = 1
	}
	for _, s2Val := range slice2 {
		m[s2Val] = m[s2Val] + 1
	}

	for mKey, mVal := range m {
		if mVal == 1 {
			diffStr = append(diffStr, mKey)
		}
	}

	return diffStr
}
