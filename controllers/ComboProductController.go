package controllers

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/helpers"
	"diluan/services"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ComboProductController interface {
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
}

type comboProductController struct {
	jwtService          services.JWTService
	comboProductService services.ComboProductService
	comboService        services.ComboService
}

func NewComboProductController(jwtService services.JWTService, comboProductService services.ComboProductService, comboService services.ComboService) ComboProductController {
	return &comboProductController{
		jwtService:          jwtService,
		comboProductService: comboProductService,
		comboService:        comboService,
	}
}

func (controller *comboProductController) Update(ctx *gin.Context) {
	var updateComboProductDTO dto.ComboProductUpdateDto
	var updateComboDTO dto.ComboUpdateDTO
	errDTO := ctx.ShouldBind(&updateComboProductDTO)
	if errDTO != nil {
		res := helpers.BuildErrorResponse("Failed to process request", errDTO.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	idParam, _ := strconv.ParseUint(ctx.Param("id"), 10, 64)

	if idParam != updateComboProductDTO.ID {
		res := helpers.BuildErrorResponse("Failed to process request", "ID does not match", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
	}

	comboProduct := controller.comboProductService.FindById(updateComboProductDTO.ID)

	if comboProduct.ID == 0 {
		res := helpers.BuildErrorResponse("Failed to process request", "Product not found", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
	}

	combo := controller.comboService.FindById(comboProduct.ComboID)
	totalPrice := combo.TotalPrice
	totalPriceProduct := comboProduct.Quantity * comboProduct.Price
	totalPrice = totalPrice - totalPriceProduct + (updateComboProductDTO.Quantity * comboProduct.Price)
	result := controller.comboProductService.Update(updateComboProductDTO)

	updateComboDTO.ID = result.ComboID
	updateComboDTO.TotalPrice = totalPrice
	controller.comboService.Update(updateComboDTO)

	res := helpers.BuildResponse(true, "Oke", result)
	ctx.JSON(http.StatusOK, res)
}

func (controller *comboProductController) Delete(ctx *gin.Context) {
	var comboProductEntities entities.ComboProduct
	var ComboUpdateDTO dto.ComboUpdateDTO
	id, _ := strconv.ParseUint(ctx.Param("id"), 10, 64)

	comboProduct:= controller.comboProductService.FindById(id)
	if comboProduct.ID == 0 {
		res := helpers.BuildErrorResponse("Combo Product Not Found" , "Combo Product Not Found" , helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest , res)
		return
	}

	combo := controller.comboService.FindById(comboProduct.ComboID)
	if combo.ID == 0 {
		res := helpers.BuildErrorResponse("Combo Not Found" , "Combo Not Found" , helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest , res)
		return
	}
	totalPrice := combo.TotalPrice
	totalPrice -= comboProduct.Price * comboProduct.Quantity
	ComboUpdateDTO.ID = combo.ID
	ComboUpdateDTO.TotalPrice = totalPrice
	controller.comboService.Update(ComboUpdateDTO)

	comboProductEntities.ID = comboProduct.ID
	controller.comboProductService.Delete(comboProductEntities)

	res := helpers.BuildResponse(true , "Oke" , "Delete Success")
	ctx.JSON(http.StatusOK , res)

}
