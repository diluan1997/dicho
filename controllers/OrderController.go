package controllers

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/helpers"
	"diluan/services"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type OrderController interface {
	All(ctx *gin.Context)
	Create(ctx *gin.Context)
	FindById(ctx *gin.Context)
	Delete(ctx *gin.Context)
}

type orderController struct {
	orderService         services.OrderService
	comboService         services.ComboService
	jwtService           services.JWTService
	orderLineItemService services.OrderLineItemService
}

type Combo struct {
	ID       uint64 `json:"id"`
}
func NewOrderController(orderService services.OrderService, comboService services.ComboService, jwtService services.JWTService, orderLineItemService services.OrderLineItemService) OrderController {
	return &orderController{
		orderService:         orderService,
		comboService:         comboService,
		jwtService:           jwtService,
		orderLineItemService: orderLineItemService,
	}
}

func (controller *orderController) All(ctx *gin.Context) {
	result := controller.orderService.All()
	res := helpers.BuildResponse(true, "Oke", result)
	ctx.JSON(http.StatusOK, res)
}

func (controller *orderController) Create(ctx *gin.Context) {
	var orderCreateDto dto.OrderCreateDTO
	orderDto := ctx.ShouldBind(&orderCreateDto)
	if orderDto != nil {
		response := helpers.BuildErrorResponse("Failed to process request", orderDto.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}

	combo_ids := strings.Split(orderCreateDto.ComboIds, ",")
	i_combo_ids := []uint64{}
	for _,id := range combo_ids {
		i_id ,_ := strconv.ParseUint(id ,10 ,64)
		i_combo_ids = append(i_combo_ids, i_id)
	}
	combos, errors := controller.comboService.FindByIds(i_combo_ids)
	if errors != nil {
		response := helpers.BuildErrorResponse("Combo Not Found", errors.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	
	user_id_chanel := make(chan string)
	go helpers.GetUserIDByToken(ctx, user_id_chanel)
	id := <-user_id_chanel
	user_id, _ := strconv.ParseUint(id, 10, 64)
	// orderToday := controller.orderService.CountOrderToDay(user_id)
	// if orderToday >= 2 {
	// 	response := helpers.BuildErrorResponse("You have created more than 2 orders today", "Create Many Order", helpers.EmptyObj{})
	// 	ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
	// 	return
	// }
	orderCreateDto.UserId = user_id
	orderCreateDto.Token = uuid.NewString()
	orderCreateDto.TotalPrice = getTotalPriceCombo(combos)
	orderCreateDto.Status = "pending"
	order := controller.orderService.Create(orderCreateDto)

	var orderLineItemDto dto.OrderLineItemCreateDTO
	for _, combo := range combos {
		orderLineItemDto.Name = combo.Name
		orderLineItemDto.Description = combo.Description
		orderLineItemDto.TotalPrice = combo.TotalPrice
		orderLineItemDto.ImageUrl = combo.ImageUrl
		orderLineItemDto.ComboID = combo.ID
		orderLineItemDto.OrderID = order.ID
		controller.orderLineItemService.Create(orderLineItemDto)
	}
	// createOrderLineItem(combos , order.ID)
	res := helpers.BuildResponse(true, "oke", order)
	ctx.JSON(http.StatusCreated, res)

}

// func (controller *orderController) GetOrderUser(ctx *gin.Context)  {
// 	id_channel := make(chan string)
// 	go getUserIDByToken1(ctx , id_channel)
// 	user_id := <- id_channel
// 	id ,_ := strconv.ParseUint(user_id , 10 ,64)
// 	result:= controller.orderService.FindById(id)
// 	res := helpers.BuildResponse(true ,"oke" , result)
// 	ctx.JSON(http.StatusOK , res)
// }

func (controller *orderController) FindById(ctx *gin.Context) {
	id := ctx.Param("id")
	order_id, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		response := helpers.BuildErrorResponse("Not Found Data", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	order := controller.orderService.FindById(order_id)
	if order.ID == 0 {
		response := helpers.BuildErrorResponse("Data Not Found", "Data Not Found", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, response)
		return
	}
	res := helpers.BuildResponse(true, "oke", order)
	ctx.JSON(http.StatusOK, res)
}

func (controller *orderController) Delete(ctx *gin.Context) {
	var order entities.Order
	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed to process request", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	getOrder := controller.orderService.FindById(id)
	if getOrder.ID == 0 {
		res := helpers.BuildErrorResponse("Data Not Found", "Data Not Found", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	order.ID = id
	controller.orderService.Delete(order)
	res := helpers.BuildResponse(true, "Oke", "Delete Success")
	ctx.JSON(http.StatusOK, res)
}

func getTotalPriceCombo(combos []entities.Combo) uint64 {
	totalPrice := 0

	for _, combo := range combos {

		totalPrice += int(combo.TotalPrice)

	}
	return uint64(totalPrice)
}

func createOrderLineItem(combos []entities.Combo, orderId uint64) {
	var orderLineItemDto dto.OrderLineItemCreateDTO
	var controller *orderController
	for _, combo := range combos {
		orderLineItemDto.ComboID = combo.ID
		orderLineItemDto.OrderID = orderId
		orderLineItemDto.TotalPrice = combo.TotalPrice
		orderLineItemDto.ImageUrl = combo.ImageUrl
		orderLineItemDto.Name = combo.Name
		orderLineItemDto.Description = combo.Description
		controller.orderLineItemService.Create(orderLineItemDto)
	}

}
