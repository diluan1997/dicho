package controllers

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/helpers"
	"diluan/services"
	"fmt"
	"net/http"
	"strconv"

	// "strconv"

	"github.com/gin-gonic/gin"
)

type AuthController interface {
	Register(ctx *gin.Context)
	VerifyAccount(ctx *gin.Context)
	Login(ctx *gin.Context)
}

type authController struct {
	authService services.AuthService
	jwtService  services.JWTService
	mailService services.MailService
}

func NewAuthController(authService services.AuthService, jwtService services.JWTService, mailService services.MailService) AuthController {
	return &authController{
		authService: authService,
		jwtService:  jwtService,
		mailService: mailService,
	}
}

func (c *authController) Login(ctx *gin.Context) {
	var loginDTO dto.LoginDTO
	err := ctx.ShouldBind(&loginDTO)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed to process request", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadGateway, res)
		return
	}
	auth := c.authService.VerifyCredential(loginDTO.Email, loginDTO.Password)
	if v, ok := auth.(entities.User); ok {
		if !v.Status {
			resVerify := helpers.BuildErrorResponse("Unverified account", "Unverified account", helpers.EmptyObj{})
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, resVerify)
			return
		}
		generateToken := c.jwtService.GenerateToken(strconv.FormatUint(v.ID, 10))
		v.AccessToken = generateToken

		res := helpers.BuildResponse(true, "OKE", v)
		ctx.JSON(http.StatusOK, res)
		return
	}

	response := helpers.BuildErrorResponse("Email or Password wrong", "Invalid credential", "")
	ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
}

func (c *authController) Register(ctx *gin.Context) {

	var registerDTO dto.RegisterDTO
	err := ctx.ShouldBind(&registerDTO)

	if err != nil {
		response := helpers.BuildErrorResponse("Failed to process request", err.Error(), "")
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, response)
		return
	}

	if !c.authService.IsDuplicateEmail(registerDTO.Email) {
		response := helpers.BuildErrorResponse("Failed to process request", "Duplicate email", "")
		ctx.JSON(http.StatusConflict, response)
	} else {
		createUser := c.authService.CreateUser(registerDTO)
		token := c.jwtService.GenerateToken(strconv.FormatUint(createUser.ID, 10))
		createUser.AccessToken = token
		go c.mailService.SendMail(createUser.Email, "Vui long xac thuc tai khoan", createUser.Token)
		
		res := helpers.BuildResponse(true, "oke", createUser)
		ctx.JSON(http.StatusCreated, res)

	}
}

func (c *authController) VerifyAccount(ctx *gin.Context) {
	var VerifyAccountDTO dto.VerifyAccountDTO
	token := ctx.Param("token")
	VerifyAccountDTO.Token = token
	errDTO := ctx.ShouldBind(&VerifyAccountDTO)
	if errDTO != nil {
		res := helpers.BuildErrorResponse("Failed to process request", errDTO.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
	}
	verify := c.authService.VerifyAccount(VerifyAccountDTO)
	fmt.Println(verify)
	if verify {
		res := helpers.BuildResponse(true, "oke", "Xác Thực Thành công")
		ctx.JSON(http.StatusCreated, res)

	} else {
		res := helpers.BuildErrorResponse("Xác Thực Tài Khoản Không Thành Công", "Tài khoản đã xác thực hoặc không tìm thấy tài khoản của bạn", helpers.EmptyObj{})
		ctx.JSON(http.StatusConflict, res)
	}

}
