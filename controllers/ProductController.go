package controllers

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/helpers"
	"diluan/services"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type ProductController interface {
	All(ctx *gin.Context)
	Create(ctx *gin.Context)
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
	FindById(ctx *gin.Context)
}

type productController struct {
	productService services.ProductService
	jwtService     services.JWTService
}

func NewProductController(productService services.ProductService, jwtService services.JWTService) ProductController {
	return &productController{
		productService: productService,
		jwtService:     jwtService,
	}
}

func (controller *productController) All(ctx *gin.Context) {

	search := ctx.Query("search")
	products := controller.productService.All(search)
	
	res := helpers.BuildResponse(true, "Oke", products)
	ctx.JSON(http.StatusOK, res)
}

func (controller *productController) Create(ctx *gin.Context) {
	var productCreateDto dto.ProductCreateDTO
	err := ctx.ShouldBind(&productCreateDto)
	fmt.Println(err)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed to process request", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	image_url := helpers.Upload(ctx)
	if !image_url.Status {
		res := helpers.BuildErrorResponse("Image Not Found", image_url.Message, helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	productSameName := controller.productService.FindByName(productCreateDto.Name)
	if !productSameName {
		res := helpers.BuildErrorResponse("Please Choose A Diffrent Name", "Product Exited", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	idChannel := make(chan string)
	go helpers.GetUserIDByToken(ctx, idChannel)
	user_id := <-idChannel
	createdBy, _ := strconv.ParseUint(user_id, 10, 64)

	productCreateDto.CreatedBy = createdBy
	productCreateDto.UpdatedBy = 0
	productCreateDto.ImageUrl = image_url.Message
	fmt.Println(productCreateDto)
	product := controller.productService.Create(productCreateDto)
	res := helpers.BuildResponse(true, "Oke", product)
	ctx.JSON(http.StatusCreated, res)
}

func (controller *productController) Update(ctx *gin.Context) {
	var productUpdateDto dto.ProductUpdateDTO
	err := ctx.ShouldBind(&productUpdateDto)
	
	if err != nil {
		res := helpers.BuildErrorResponse("Failed to process request", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	idParam, _ := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if idParam != productUpdateDto.ID {
		res := helpers.BuildErrorResponse("Product not found", "Please check param and payload", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	product := controller.productService.CheckById(idParam)
	if !product {
		res := helpers.BuildErrorResponse("Product not found", "Please check param and payload", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	productSameName := controller.productService.FindNameDiffId(idParam, productUpdateDto.Name)
	if !productSameName {
		res := helpers.BuildErrorResponse("Duplicate name", "Please choose a differnt name", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}

	_, errFile := ctx.FormFile("image_url")
	if errFile == nil {
		image_url := helpers.Upload(ctx)
		productUpdateDto.ImageUrl = image_url.Message
	}
	idChannel := make(chan string)
	go helpers.GetUserIDByToken(ctx, idChannel)
	user_id := <-idChannel
	id, _ := strconv.ParseUint(user_id, 10, 64)
	productUpdateDto.UpdatedBy = id
	fmt.Println(productUpdateDto.Status)
	result := controller.productService.Update(productUpdateDto)
	if productUpdateDto.Status == false {
		controller.productService.UpdateStatus(productUpdateDto)
	}
	res := helpers.BuildResponse(true, "Oke", result)
	ctx.JSON(http.StatusOK, res)
}

func (controller *productController) Delete(ctx *gin.Context) {
	var product entities.Product

	id, _ := strconv.ParseUint(ctx.Param("id"), 10, 64)

	productById := controller.productService.CheckById(id)
	if !productById {
		res := helpers.BuildErrorResponse("Product not found", "Please check param and payload", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	product.ID = id
	controller.productService.Delete(product)
	res := helpers.BuildResponse(true, "Oke", "Delete Success")
	ctx.JSON(http.StatusOK, res)
}

func (controller *productController) FindById(ctx *gin.Context) {

	id, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err != nil {
		res := helpers.BuildErrorResponse("Failed process to request", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	product := controller.productService.FindById(id)
	if product.ID == 0 {
		res := helpers.BuildErrorResponse("Product not found", "Please check again", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	res := helpers.BuildResponse(true, "Oke", product)
	ctx.JSON(http.StatusOK, res)
}
