package controllers

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/helpers"
	"diluan/services"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type UserController interface {
	All(ctx *gin.Context)
	Me(ctx *gin.Context)
	FindById(ctx *gin.Context)
	Update(ctx *gin.Context)
	Delete(ctx *gin.Context)
}

type userController struct {
	userService services.UserService
	jwtService  services.JWTService
}

func NewUserController(userService services.UserService, jwtService services.JWTService) UserController {
	return &userController{
		userService: userService,
		jwtService:  jwtService,
	}
}

func (controller *userController) All(ctx *gin.Context) {
	var users []entities.User = controller.userService.All()
	res := helpers.BuildResponse(true, "OKE", users)
	ctx.JSON(http.StatusOK, res)
}

func (controller *userController) Me(ctx *gin.Context) {
	authHeader := ctx.GetHeader("Authorization")
	fmt.Println(authHeader)
	token, err := controller.jwtService.ValidateToken(authHeader)
	if err != nil {
		panic(err.Error())
	}
	claims := token.Claims.(jwt.MapClaims)
	userID := fmt.Sprintf("%v", claims["user_id"])
	id, err := strconv.ParseUint(userID, 10, 64)
	if err != nil {
		resErr := helpers.BuildErrorResponse("Không tìm thấy người dùng", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, resErr)
		return
	}
	user := controller.userService.FindById(id)
	res := helpers.BuildResponse(true, "OKE", user)
	ctx.JSON(http.StatusOK, res)
}

func (controller *userController) FindById(ctx *gin.Context) {
	userID, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		resErr := helpers.BuildErrorResponse("Không tìm thấy người dùng", err.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, resErr)
		return
	}
	var user entities.User = controller.userService.FindById(userID)
	if idNull := user.ID; idNull == 0 {
		resX := helpers.BuildErrorResponse("Không tìm thấy người dùng", "Không tìm thấy người dùng", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, resX)
		return
	}
	res := helpers.BuildResponse(true, "OKE", user)
	ctx.JSON(http.StatusOK, res)
}

func (controller *userController) Update(ctx *gin.Context) {
	var userDTO dto.UpdateUserDTO
	errDto := ctx.ShouldBind(&userDTO)
	if errDto != nil {
		res := helpers.BuildErrorResponse("Failed to process request ", errDto.Error(), helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, res)
		return
	}
	idParam, err := strconv.ParseUint(ctx.Param("id"), 10, 64)
	if err == nil && idParam == userDTO.ID {
		user := controller.userService.FindById(idParam)
		if idNull := user.ID; idNull == 0 {
			resX := helpers.BuildErrorResponse("Không tìm thấy người dùng", "Không tìm thấy người dùng", helpers.EmptyObj{})
			ctx.AbortWithStatusJSON(http.StatusBadRequest, resX)
			return
		}
		userDTO.UpdatedAt = time.Now()
		result := controller.userService.Update(userDTO)
		res := helpers.BuildResponse(true, "oke", result)
		ctx.JSON(http.StatusOK, res)
	} else {
		resX := helpers.BuildErrorResponse("Không tìm thấy người dùng", "Không tìm thấy người dùng", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, resX)
		return
	}
}

func (controller *userController) Delete(ctx *gin.Context) {
	var user entities.User
	id, err := strconv.ParseUint(ctx.Param("id"), 0, 0)
	if err != nil {
		response := helpers.BuildErrorResponse("Không tìm thấy id người dùng", "Không tìm thấy id người dùng", helpers.EmptyObj{})
		ctx.JSON(http.StatusBadRequest, response)
	}
	userCheck := controller.userService.FindById(id)
	if idNull := userCheck.ID; idNull == 0 {
		resX := helpers.BuildErrorResponse("Không tìm thấy người dùng", "Không tìm thấy người dùng", helpers.EmptyObj{})
		ctx.AbortWithStatusJSON(http.StatusBadRequest, resX)
		return
	}
	user.ID = id
	controller.userService.Delete(user)
	res := helpers.BuildResponse(true, "oke", helpers.EmptyObj{})
	ctx.JSON(http.StatusOK, res)

}

func (controller *userController) getUserIDByToken(token string) string {
	aToken, err := controller.jwtService.ValidateToken(token)
	if err != nil {
		panic(err.Error())
	}
	claims := aToken.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	return id
}
