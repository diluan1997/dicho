# Phần người dùng
    * Đăng nhập
    * Đăng kí
    * Đặt đơn đi chợ theo combo
    * Cập nhật thông tin người dùng

# Phần quản lý 
    * Quản lý đơn đi chợ ng dùng đặt
    * Quản lý combo đặt hàng
    * Quản lý người dùng 
    

# Phần api 
    * Api users 
        1. 
            #### GET 
            ***/v1/api/users   - Lấy tất cả User
            *param* :  no

        2. 
            #### GET
            ***/v1/api/users/:id   - Lấy user theo id
            *param* :  no

        3. 
            #### PATCH
            ***/v1/api/users/:id   - Update user theo id
            *param* :  id,name,phone

        4. 
            #### DELETE
            ***/v1/api/users/:id   - Xoá user theo id
            *param* :  id,name,phone

    * Api combos 
        1. 
            #### GET 
            ***/v1/api/combos   - Lấy tất cả combo
            *param* :  no

        2. 
            #### GET
            ***/v1/api/combos/:id   - Lấy combo theo id
            *param* :  no

        3. 
            #### POST
            ***/v1/api/combos   - Create combo
            *param* :  name,description, status  , image_url(files[0]) , line_items(
                [
                    {
                        id : 1,
                        quantity : 1
                    },{
                        id : 2,
                        quantity : 1
                    }
                ]
            ) 

        4. 
            #### PATCH
            ***/v1/api/combos/:id   - Update combo theo id
            *param* :  id,name,description, status  , image_url ( id thi bat buoc , 5 cai kia optional ) ,  line_items(
                [
                    {
                        id : 1,
                        quantity : 1
                    },{
                        id : 2,
                        quantity : 1
                    }
                ]
            ) 
        5. 
            #### DELETE
            ***/v1/api/combos/:id   - Xoá combo theo id

        6. 
            #### PATCH
            ***/v1/api/combo-products/:id  - Cập Nhật số lượng hoặc giảm só lượng sp của combo ( không thể truyền quantity = 0 lên)
            *param* :  id,quantity
        7. 
            #### POST
            ***//v1/api/combos/:id/products  (:id id của combo)  - Thêm Sản phẩm vào combo
            *param* :  combo_id, product_id,quantity
        8. 
            #### DELETE
            ***/v1/api/combo-products/:id  - Xoá sản phẩm của combo
            

    * Api products 
        1. 
            #### GET 
            ***/v1/api/products   - Lấy tất cả products
            ***/v1/api/products?search=   - search products
            *param* :  no

        2. 
            #### GET
            ***/v1/api/products/:id   - Lấy products theo id
            *param* :  no

        3. 
            #### POST
            ***/v1/api/products   - Create products
            *param* :  name,description, status , price , image_url(files[0]) , quantity , unit ( cai , kg , ....)

        4. 
            #### PATCH
            ***/v1/api/products/:id   - Update products theo id
            *param* :  id,name,description, status , price , image_url ,quantity , unit ( cai , kg , ....) ( id thi bat buoc , 5 cai kia optional )


        5. 
            #### DELETE
            ***/v1/api/products/:id   - Xoá products theo id
            *param* :  no


    * Api orders 
        1. 
            #### GET 
            ***/v1/api/orders   - Lấy tất cả orders
            *param* :  no

        2. 
            #### GET
            ***/v1/api/orders/:id   - Lấy orders theo id
            *param* :  no

        4. 
            #### POST
            ***/v1/api/orders   - Tạo orders
            *param* :  id,name,description, status , price ( id thi bat buoc , 4 cai kia optional)


        5. 
            #### DELETE
            ***/v1/api/orders/:id   - Xoá orders theo id
            *param* :  id,name,phone