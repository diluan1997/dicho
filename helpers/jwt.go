package helpers

import (
	"diluan/services"
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type JWT struct {
	UserID string
}

func GetUserIDByToken(ctx *gin.Context, idChannel chan string) JWT {
	token := ctx.GetHeader("Authorization")
	aToken, err := services.NewJWTService().ValidateToken(token)
	if err != nil {
		panic(err.Error())
	}
	claims := aToken.Claims.(jwt.MapClaims)
	id := fmt.Sprintf("%v", claims["user_id"])
	idChannel <- id
	return JWT{
		UserID: id,
	}
}
