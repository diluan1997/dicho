package helpers

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

type Image struct {
	Status  bool   `json:"status"`
	Message string `json:"message"`
}

func Upload(c *gin.Context) Image {
	file, err := c.FormFile("image_url")
	if err != nil {
		res := Image{
			Status:  false,
			Message: "Image Not Found, Please Choose a Iamge",
		}
		return res
	}
	today := time.Now().Format("2006-01-02")

	// check exention Image
	extension := strings.ToLower(string(filepath.Ext(file.Filename)))
	extension_vaidate := []string{".jpg", ".png", ".gif", ".jpeg"}
	_, found := Find(extension_vaidate, extension)
	if !found {
		return Image{
			Status:  false,
			Message: "The image is not in the correct format : .jpg, .png, .gif, .jpeg",
		}
	}
	// Check size Image
	size := file.Size
	if size > 1024*2048 {
		return Image{
			Status:  false,
			Message: "Images larger than 2MB",
		}
	}
	// Check folder and create if not exit
	base_path := "./uploads/images/" + today
	if _, err := os.Stat(base_path); os.IsNotExist(err) {
		os.Mkdir(base_path, os.ModePerm)
	}
	// Generate Name and Save Image
	filename := fmt.Sprint(time.Now().UnixNano()) + extension
	image_url := "uploads/images/" + today + "/" + filename
	dst := path.Join(base_path, filename)
	if err := c.SaveUploadedFile(file, dst); err != nil {
		return Image{
			Status:  false,
			Message: err.Error(),
		}

	}
	return Image{
		Status:  true,
		Message: image_url,
	}
}

func Find(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}
