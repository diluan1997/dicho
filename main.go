package main

import (
	"diluan/config"
	"diluan/controllers"
	"diluan/middleware"
	"diluan/repositories"
	"diluan/services"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	db                     *gorm.DB                            = config.SetUpDatabaseConnection()
	userRepository         repositories.UserRepository         = repositories.NewUserRepository(db)
	orderRepository        repositories.OrderRepository        = repositories.NewOrderRepository(db)
	comboRepository        repositories.ComboRepository        = repositories.NewComboRepository(db)
	productRepository      repositories.ProductRepository      = repositories.NewProductRepository(db)
	comboProductRepository repositories.ComboProductRepository = repositories.NewComboProductRepository(db)
	orderLineItemRepository repositories.OrderLineItemRepository = repositories.NewOrderLineItemRepository(db)

	authService         services.AuthService         = services.NewAuthService(userRepository)
	userService         services.UserService         = services.NewUserSerive(userRepository)
	orderService        services.OrderService        = services.NewOrderService(orderRepository)
	comboService        services.ComboService        = services.NewComboService(comboRepository)
	productService      services.ProductService      = services.NewProductService(productRepository)
	comboProductService services.ComboProductService = services.NewComboProductService(comboProductRepository)
	jwtService          services.JWTService          = services.NewJWTService()
	mailService         services.MailService         = services.NewMailService()
	orderLineItemService services.OrderLineItemService = services.NewOrderLineItemService(orderLineItemRepository)

	comboProductController controllers.ComboProductController = controllers.NewComboProductController(jwtService, comboProductService, comboService)
	authcontroller         controllers.AuthController         = controllers.NewAuthController(authService, jwtService, mailService)
	userController         controllers.UserController         = controllers.NewUserController(userService, jwtService)
	comboController        controllers.ComboController        = controllers.NewComboController(comboService, jwtService, productService, comboProductService)
	productController      controllers.ProductController      = controllers.NewProductController(productService, jwtService)
	orderController        controllers.OrderController        = controllers.NewOrderController(orderService, comboService, jwtService ,orderLineItemService)

)

// @title User API documentation
// @version 1.0.0
// @host localhost:5000
// @BasePath /user

func main() {
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()

	router.GET("", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Oke",
		})
	})
	router.Static("uploads", "./uploads")
	authRoutes := router.Group("/v1/api/auth")
	{
		authRoutes.POST("/register", authcontroller.Register)
		authRoutes.POST("/login", authcontroller.Login)
		authRoutes.GET("/verify/:token", authcontroller.VerifyAccount)
	}

	userRoutes := router.Group("/v1/api/users", middleware.AuthorizeJWT(jwtService))
	{
		userRoutes.GET("/", userController.All)
		userRoutes.GET("/me", userController.Me)
		userRoutes.GET("/:id", userController.FindById)
		userRoutes.PUT("/:id", userController.Update)
		userRoutes.DELETE("/:id", userController.Delete)
	}
	orderRoutes := router.Group("/v1/api/orders", middleware.AuthorizeJWT(jwtService))
	{
		orderRoutes.GET("/", orderController.All)
		orderRoutes.POST("/", orderController.Create)
		orderRoutes.GET("/:id", orderController.FindById)
		// orderRoutes.POST("/:id", userController.Update)
		orderRoutes.DELETE("/:id", orderController.Delete)
	}

	comboRoutes := router.Group("/v1/api/combos", middleware.AuthorizeJWT(jwtService))
	{
		comboRoutes.GET("/", comboController.All)
		comboRoutes.POST("/", comboController.Create)
		comboRoutes.POST("/:id/products", comboController.CreateProduct)
		comboRoutes.GET("/:id", comboController.FindById)
		comboRoutes.PATCH("/:id", comboController.Update)
		comboRoutes.DELETE("/:id", comboController.Delete)
	}
	comboProductRoutes := router.Group("/v1/api/combo-products", middleware.AuthorizeJWT(jwtService))
	{
		comboProductRoutes.PATCH("/:id", comboProductController.Update)
		comboProductRoutes.DELETE("/:id", comboProductController.Delete)
	}

	productRoutes := router.Group("/v1/api/products", middleware.AuthorizeJWT(jwtService))
	{
		productRoutes.GET("/", productController.All)
		productRoutes.POST("/", productController.Create)
		productRoutes.GET("/:id", comboController.FindById)
		productRoutes.PATCH("/:id", productController.Update)
		productRoutes.DELETE("/:id", productController.Delete)
	}

	router.Run(":9990")
}
