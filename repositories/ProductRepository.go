package repositories

import (
	"diluan/entities"

	"gorm.io/gorm"
)

type ProductRepository interface {
	All(search string) []entities.Product
	Create(product entities.Product) entities.Product
	Update(product entities.Product) entities.Product
	UpdateStatus(product entities.Product)
	FindByName(name string) bool
	CheckById(id uint64) bool
	FindById(id uint64) entities.Product
	FindByIds(IDs []string) []entities.Product
	FindNameDiffId(id uint64, name string) bool
	Delete(product entities.Product)
}

type productRepository struct {
	connection *gorm.DB
}

func NewProductRepository(db *gorm.DB) ProductRepository {
	return &productRepository{
		connection: db,
	}
}

func (db *productRepository) All(search string) []entities.Product {
	var products []entities.Product
	data := db.connection.Scopes(SelectFieldProductScope).Preload("User")
	if search != "" {
		data.Where("name LIKE ?", "%"+search+"%")
	}
	data.Find(&products)
	return products
}

func (db *productRepository) FindByIds(IDs []string) []entities.Product {
	var products []entities.Product
	db.connection.
		Where("id IN ?" , IDs).
		Scopes(SelectFieldProductScope).
		Find(&products)
	return products
}

func (db *productRepository) Create(product entities.Product) entities.Product {
	db.connection.Save(&product)
	return product
}

func (db *productRepository) UpdateStatus(product entities.Product) {
	db.connection.Model(&product).Debug().Updates(map[string]interface{}{"id" : product.ID , "status" : false})
}

func (db *productRepository) Update(product entities.Product) entities.Product {
	db.connection.Debug().Updates(&product)
	db.connection.Scopes(SelectFieldProductScope).Preload("User").Find(&product)
	return product
}

func (db *productRepository) FindByName(name string) bool {
	var product entities.Product
	if err := db.connection.Scopes(SelectDefaultScope).Where("name = ?", name).First(&product); err.Error != nil {
		return true
	}
	return false
}

func (db *productRepository) CheckById(ID uint64) bool {
	var product entities.Product
	if err := db.connection.Scopes(SelectFieldProductScope).Find(&product, ID); err.Error != nil {
		return false
	}
	return true
}

func (db *productRepository) FindById(ID uint64) entities.Product {
	var product entities.Product
	db.connection.Scopes(SelectFieldProductScope).Find(&product, ID)
	return product
}

func (db *productRepository) FindNameDiffId(id uint64, name string) bool {
	var product entities.Product
	if err := db.connection.Scopes(SelectFieldProductScope).Where("id != ? and name = ?", id, name).Take(&product); err.Error != nil {
		return true
	}
	return false
}

func (db *productRepository) Delete(product entities.Product) {
	db.connection.Delete(&product)
}

func SelectFieldProductScope(db *gorm.DB) *gorm.DB {
	var fillable = []string{"id", "name", "description", "price", "status", "image_url", "created_by", "updated_by", "created_at", "updated_at","deleted_at" , "unit" , "quantity"}
	return db.Select(fillable).Unscoped()
}
