package repositories

import (
	"diluan/entities"

	"gorm.io/gorm"
)

type OrderLineItemRepository interface{
	Create(orderLineItem entities.OrderLineItem) entities.OrderLineItem
}

type orderLineItemRepository struct{
	connection *gorm.DB
}

func NewOrderLineItemRepository(db *gorm.DB) OrderLineItemRepository {
	return &orderLineItemRepository{
		connection : db,
	}
}

func (db *orderLineItemRepository) Create(orderLineItem entities.OrderLineItem) entities.OrderLineItem{
	db.connection.Debug().Create(&orderLineItem)
	return orderLineItem
}