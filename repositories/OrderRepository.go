package repositories

import (
	"diluan/entities"
	"time"

	"gorm.io/gorm"
)

type OrderRepository interface {
	All() []entities.Order
	Create(order entities.Order) entities.Order
	CountOrderToDay(id uint64) uint64
	FindById(id uint64) entities.Order
	Delete(order entities.Order)
}

type orderRepository struct {
	connection *gorm.DB
}

func NewOrderRepository(db *gorm.DB) OrderRepository {
	return &orderRepository{
		connection: db,
	}
}

func (db *orderRepository) CountOrderToDay(id uint64) uint64 {
	today := time.Now()
	start_date := today.Format("2006-01-02") + " 00:00:00"
	end_date := today.Format("2006-01-02") + " 23:59:59"
	var order []entities.Order
	result := db.connection.Where("user_id = ?", id).Where("created_at BETWEEN ? AND ?", start_date, end_date).Find(&order)
	count := result.RowsAffected
	return uint64(count)

}

func (db *orderRepository) FindById(id uint64) entities.Order {
	var order entities.Order
	db.connection.Preload("OrderLineItem").Preload("User").Find(&order, id)
	return order
}

func (db *orderRepository) All() []entities.Order {
	var order []entities.Order
	db.connection.Scopes(SelectFieldsOrderScope).Preload("User").Preload("User").Preload("OrderLineItem").Find(&order)
	return order
}

func (db *orderRepository) Create(order entities.Order) entities.Order {
	db.connection.Debug().Save(&order)
	db.connection.Preload("Combo.User").Preload("User").Find(&order)
	return order
}

func (db *orderRepository) Delete(order entities.Order) {
	db.connection.Delete(&order)
}

func SelectFieldsOrderScope(db *gorm.DB) *gorm.DB {
	return db.Debug().Select("id", "token", "user_id", "total_price","note" , "user_id" , "created_at", "updated_at")
}
