package repositories

import (
	"diluan/entities"

	"gorm.io/gorm"
)

type ComboRepository interface {
	All() []entities.Combo
	Create(combo entities.Combo) entities.Combo
	Update(combo entities.Combo) entities.Combo
	UpdateStatus(combo entities.Combo)
	FindByName(name string) (tx *gorm.DB)
	FindById(id uint64) entities.Combo
	FindByIds(IDs []uint64) ([]entities.Combo , error)
	FindByNameUpdate(id int64, name string) (tx *gorm.DB)
	Delete(combo entities.Combo)
}

type comboRepository struct {
	connection *gorm.DB
}

func NewComboRepository(db *gorm.DB) ComboRepository {
	return &comboRepository{
		connection: db,
	}
}

func (db *comboRepository) All() []entities.Combo {
	var combos []entities.Combo
	db.connection.Debug().
		Scopes(SelectDefaultScope).
		Unscoped().
		Preload("User").
		Preload("ComboProduct").
		Find(&combos)
	return combos
}


func (db *comboRepository) Delete(combo entities.Combo) {
	db.connection.Scopes(SelectDefaultScope).Delete(&combo)
}

func (db *comboRepository) FindById(id uint64) entities.Combo {
	var combo entities.Combo
	db.connection.
		Unscoped().
		Where("id = ? ", id).
		Preload("User").
		Preload("ComboProduct").
		Take(&combo)
	return combo
}

func (db *comboRepository) FindByIds(IDs []uint64) ([]entities.Combo , error) {
	var combo []entities.Combo
	if result := db.connection.
		Unscoped().
		Debug().
		Where("id IN ?", IDs).
		Find(&combo); result.Error != nil {
			return nil , result.Error
		}

	return combo , nil
}

func (db *comboRepository) FindByName(name string) (tx *gorm.DB) {
	var combo entities.Combo
	return db.connection.
		Select("id", "name", "description", "price", "created_at", "updated_at", "created_by", "image_url").
		Unscoped().
		Where("name = ?", name).
		Take(&combo)

}

func (db *comboRepository) FindByNameUpdate(id int64, name string) (tx *gorm.DB) {
	var combo entities.Combo
	return db.connection.
		Scopes(SelectDefaultScope).
		Where("name = ?", name).
		Where("id != ?", id).
		Where("deleted_at IS NULL").
		Take(&combo)
}

func (db *comboRepository) Create(combo entities.Combo) entities.Combo {
	db.connection.Save(&combo)
	return combo
}

func (db *comboRepository) Update(combo entities.Combo) entities.Combo {
	db.connection.Unscoped().Updates(&combo)
	db.connection.Unscoped().
		Scopes(SelectDefaultScope).
		Preload("User").
		Preload("ComboProduct").
		Find(&combo)
	return combo
}

func (db *comboRepository) UpdateStatus(combo entities.Combo) {
	db.connection.Model(&combo).Debug().Updates(map[string]interface{}{"id" : combo.ID , "status" : false})
}


func SelectDefaultScope(db *gorm.DB) *gorm.DB {
	return db.Select("id", "name", "description", "total_price", "created_at", "updated_at", "created_by", "image_url", "deleted_at" ,"status")
}
