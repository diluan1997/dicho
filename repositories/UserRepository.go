package repositories

import (
	"diluan/entities"
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/segmentio/ksuid"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository interface {
	All() []entities.User
	FindById(Id uint64) entities.User
	VerifyCredential(email string, password string) interface{}
	InsertUser(user entities.User) entities.User
	IsDuplicateEmail(email string) (tx *gorm.DB)
	VerifyUSer(token string) bool
	Update(user entities.User) entities.User
	Delete(user entities.User)
}

type userConnection struct {
	connection *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userConnection{
		connection: db,
	}
}

func (db *userConnection) Update(user entities.User) entities.User {
	fmt.Println(&user)
	db.connection.Debug().Updates(&user)
	db.connection.Debug().Find(&user)
	return user
}

func (db *userConnection) Delete(user entities.User) {
	db.connection.Delete(&user)
}

func (db *userConnection) FindById(Id uint64) entities.User {
	var user entities.User
	db.connection.Find(&user, Id)
	return user
}

func (db *userConnection) All() []entities.User {
	var users []entities.User
	db.connection.Select("id", "name", "email", "created_at", "role", "phone", "status").Order("id desc").Find(&users)
	return users
}

func (db *userConnection) VerifyCredential(email string, password string) interface{} {
	var user entities.User
	res := db.connection.Select("name", "id", "phone", "status", "access_token", "email", "password", "role").Where("email =?", email).Take(&user)
	if res.Error == nil {
		return user
	}
	return nil
}

func (db *userConnection) InsertUser(user entities.User) entities.User {
	user.Password = hashAndSalt([]byte(user.Password))
	token := ksuid.New()
	user.Token = strings.ToUpper(string(token.String()))
	db.connection.Save(&user)
	return user
}

func (db *userConnection) IsDuplicateEmail(email string) (tx *gorm.DB) {
	var user entities.User
	return db.connection.Where("email = ?", email).Take(&user)
}

func (db *userConnection) VerifyUSer(token string) bool {
	// var tempUser entities.User
	var user entities.User
	getUser := db.connection.Where("token = ?", token).Take(&user)
	if getUser.Error == nil {
		user.Status = true
		user.UpdatedAt = time.Now()
		user.Token = ""
		db.connection.Save(&user)
		return true
	}
	return false
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
		panic("Failed to hash a password")
	}
	return string(hash)
}
