package repositories

import (
	"diluan/entities"
	"gorm.io/gorm"
)

type ComboProductRepository interface{
	Create(comboProduct entities.ComboProduct) entities.ComboProduct
	Update(comboProduct entities.ComboProduct) entities.ComboProduct
	Delete(comboProduc entities.ComboProduct)
	FindById(ID uint64) entities.ComboProduct
	FindByProductAndComboID(ProductID uint64 , ComboID uint64) entities.ComboProduct
}

type comboProductRepository struct{
	connection *gorm.DB
}

func NewComboProductRepository(db *gorm.DB) ComboProductRepository {
	return &comboProductRepository{
		connection: db,
	}
}

func (db *comboProductRepository) Create(comboProduct entities.ComboProduct) entities.ComboProduct {
	db.connection.Debug().Save(&comboProduct)
	return comboProduct
}

func (db *comboProductRepository) Update(comboProduct entities.ComboProduct) entities.ComboProduct {
	db.connection.Updates(&comboProduct)
	db.connection.Unscoped().
		Find(&comboProduct)
	return comboProduct
}

func (db *comboProductRepository) Delete(comboProduct entities.ComboProduct) {
	db.connection.Debug().Where("id = ?" , comboProduct.ID).Or("product_id = ?", comboProduct.ProductID).Delete(&comboProduct)
}

func (db *comboProductRepository) FindById(ID uint64) entities.ComboProduct  {
	var comboProduct entities.ComboProduct
	db.connection.Scopes(SelectFieldComboProducttScope).Where("id = ?" , ID).Take(&comboProduct)
	return comboProduct
}

func (db *comboProductRepository) FindByProductAndComboID(ProductID uint64 , ComboID uint64) entities.ComboProduct {
	var comboProduct entities.ComboProduct
	db.connection.Scopes(SelectFieldComboProducttScope).
			Where("product_id" , ProductID).
			Where("combo_id" , ComboID).
			Take(&comboProduct)
	return comboProduct

}

func SelectFieldComboProducttScope(db *gorm.DB) *gorm.DB {
	return db.Debug().Select("id", "combo_id", "product_id", "name", "price", "created_at", "updated_at", "status", "unit" , "quantity")
}


