package entities

import (
	"time"
)

type User struct {
	ID          uint64    `gorm:"primary_key:auto_increment" json:"id"`
	Name        string    `gorm:"index:idx_name,uniqe,type:varchar(255)" json:"name"`
	Email       string    `gorm:"index:idx_email,unique,type:varchar(255)" json:"email"`
	Phone       string    `gorm:"index:idx_email,unique,type:varchar(255)" json:"phone"`
	Password    string    `gorm:"->;<-;not null" json:"-"`
	Status      bool      `gorm:"default(1)" json:"status"`
	Token       string    `gorm:"type:varchar(255)" json:"token"`
	AccessToken string    `json:"access_token"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Role        string    `gorm:"type:varchar(255)" json:"role"`
}
