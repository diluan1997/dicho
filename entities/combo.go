package entities

import (
	"time"

	"gorm.io/gorm"
)

type Combo struct {
	ID           uint64         `gorm:"primary_key:auto_increment" json:"id"`
	Name         string         `gorm:"index;unique;type:varchar(255)" json:"name"`
	Description  string         `json:"description"`
	TotalPrice   uint64         `gorm:"not null" json:"total_price"`
	CreatedBy    uint64         `gorm:"not null" json:"created_by"`
	UpdatedBy    uint64         `gorm:"not null" json:"-"`
	Status       bool           `gorm:"default:0" json:"status"`
	ImageUrl     string         `gorm:"type:varchar(255)" json:"image_url"`
	CreatedAt    time.Time      `json:"created_at"`
	UpdatedAt    time.Time      `json:"updated_at"`
	DeletedAt    gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	User         User           `gorm:"foreignKey:CreatedBy" json:"creator"`
	ComboProduct []ComboProduct `gorm:"foreignKey:ComboID" json:"products"`
}
