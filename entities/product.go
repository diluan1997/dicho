package entities

import (
	"time"

	"gorm.io/gorm"
)

type Product struct {
	ID          uint64         `gorm:"primary_key:auto_increment" json:"id"`
	Name        string         `gorm:"index;unique;type:varchar(255)" json:"name"`
	Description string         `json:"description"`
	Price       uint64         `gorm:"not null" json:"price"`
	CreatedBy   uint64         `gorm:"not null" json:"created_by"`
	UpdatedBy   uint64         `gorm:"not null" json:"-"`
	Quantity    uint64         `gorm:"not null" json:"quantity"`
	Unit        string         `gorm:"type:varchar(255)" json:"unit"`
	Status      bool           `gorm:"type:boolean; column:status" json:"status"`
	ImageUrl    string         `gorm:"type:varchar(255)" json:"image_url"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	User        User           `gorm:"foreignKey:CreatedBy" json:"creator"`
}
