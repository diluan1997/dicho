package entities

import (
	"time"

	"gorm.io/gorm"
)

type Order struct {
	ID            uint64          `gorm:"primary_key:auto_increment" json:"id"`
	Token         string          `gorm:"type:varchar(255)" json:"token"`
	TotalPrice    uint64          `gorm:"not null" json:"total_price"`
	CreatedAt     time.Time       `json:"created_at"`
	UpdatedAt     time.Time       `json:"updated_at"`
	DeletedAt     gorm.DeletedAt  `gorm:"index" json:"deleted_at"`
	CanceledAt    *time.Time       `gorm:"index" json:"canceled_at"`
	Note          string          `gorm:"type:varchar(255)" json:"note"`
	UserId        uint64          `gorm:"not null" json:"user_id"`
	User          User            `gorm:"foreignKey:UserId" json:"user"`
	Status        string          `gorm:"type:varchar(10)" json:"status"`
	OrderLineItem []OrderLineItem `gorm:"foreignKey:OrderID"  json:"line_items"`
}
