package entities

import (
	"time"
)

type ComboProduct struct {
	ID        uint64    `gorm:"primary_key:auto_increment" json:"id"`
	ComboID   uint64    `gorm:"index,not null" json:"combo_id"`
	ProductID uint64    `gorm:"index,not null" json:"product_id"`
	Name      string    `gorm:"index;type:varchar(255)" json:"name"`
	Price     uint64    `gorm:"not null" json:"price"`
	Quantity  uint64    `gorm:"not null" json:"quantity"`
	Unit      string    `gorm:"not null" json:"unit"`
	Status    bool      `gorm:"default:0" json:"status"`
	ImageUrl  string    `gorm:"type:varchar(255)" json:"image_url"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
