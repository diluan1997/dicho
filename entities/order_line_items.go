package entities

type OrderLineItem struct {
	ID          uint64 `gorm:"primary_key;auto_increment" json:"id"`
	ComboID     uint64 `gorm:"index,not null" json:"combo_id"`
	OrderID     uint64 `gorm:"index,not null" json:"order_id"`
	Name        string `gorm:"type:varchar(255)" json:"name"`
	Description string `json:"description"`
	TotalPrice  uint64 `gorm:"not null" json:"total_price"`
	ImageUrl    string `gorm:"type:varchar(255)" json:"image_url"`
}
