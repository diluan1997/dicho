package config

import (
	"diluan/entities"
	"fmt"
	"log"
	"os"

	// "path/filepath"

	// "os"

	// "os"
	"time"

	// "github.com/joho/godotenv"
	// "github.com/joho/godotenv"
	// "github.com/joho/godotenv"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type User struct {
	Name      string
	Email     string
	Password  string
	Status    bool
	Role      string
	Phone     string
	CreatedAt time.Time
	UpdatedAt time.Time
}

var user = []User{{
	Name:     "Admin",
	Email:    "admin@gmail.com",
	Password: hashAndSalt([]byte("123123")),
	Status:   true,
	Role:     "admin",
	Phone:    "0123123123",
}}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
		panic("Failed to hash a password")
	}
	return string(hash)
}

func SetUpDatabaseConnection() *gorm.DB {
	// dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// environmentPath := filepath.Join(dir, ".env")
	err = godotenv.Load(environmentPath)
	dbUser := os.Getenv("DB_USER") // "root"
	dbPass :=  os.Getenv("DB_PASS") // "diluan97"
	dbHost := os.Getenv("DB_HOST") //"127.0.0.1"
	dbName := os.Getenv("DB_NAME") // "first_go"

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,   // Slow SQL threshold
			LogLevel:                  logger.Silent, // Log level
			IgnoreRecordNotFoundError: true,          // Ignore ErrRecordNotFound error for logger
			Colorful:                  false,         // Disable color
		},
	)
	dsn := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8mb4&parseTime=True&loc=Local", dbUser, dbPass, dbHost, dbName)
	db, error := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: newLogger,
	})

	if error != nil {
		panic("Failed to creat connection database")
	}
	// db.Create(&user)
	db.AutoMigrate(&entities.User{}, &entities.Order{}, &entities.Combo{}, &entities.Product{} ,&entities.ComboProduct{}, entities.OrderLineItem{})

	return db

}

func CloseDatabaseConnection(db *gorm.DB) {
	dbSql, err := db.DB()
	if err != nil {
		panic("Failed to close connection database")
	}
	dbSql.Close()
}
