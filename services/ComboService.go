package services

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/repositories"
	"log"

	"github.com/mashingan/smapping"
)

type ComboService interface {
	All() []entities.Combo
	Create(combo dto.ComboCreateDTO) entities.Combo
	Update(combo dto.ComboUpdateDTO) entities.Combo
	UpdateStatus(comboDto dto.ComboUpdateDTO)
	Delete(combo entities.Combo)
	FindById(ID uint64) entities.Combo
	FindByIds(IDs []uint64) ([]entities.Combo , error)
	FindByNameUpdate(id int64, name string) bool
}

type comboService struct {
	comboRepository repositories.ComboRepository
}

func NewComboService(comboRepository repositories.ComboRepository) ComboService {
	return &comboService{
		comboRepository: comboRepository,
	}
}

func (cb *comboService) All() []entities.Combo {
	return cb.comboRepository.All()
}



func (cb *comboService) Delete(combo entities.Combo) {
	cb.comboRepository.Delete(combo)
}

func (cb *comboService) Create(comboDto dto.ComboCreateDTO) entities.Combo {
	combo := entities.Combo{}
	err := smapping.FillStruct(&combo, smapping.MapFields(&comboDto))
	if err != nil {
		log.Fatalf("Failed map %v", err)
	}
	res := cb.comboRepository.Create(combo)
	return res
}

func (cb *comboService) FindByNameUpdate(id int64, name string) bool {
	res := cb.comboRepository.FindByNameUpdate(id, name)
	return !(res.Error == nil)
}

func (cb *comboService) FindById(ID uint64) entities.Combo {
	return cb.comboRepository.FindById(ID)
}

func (cb *comboService) FindByIds(IDs []uint64) ([]entities.Combo ,error) {
	return cb.comboRepository.FindByIds(IDs)
}
func (cb *comboService) Update(comboDto dto.ComboUpdateDTO) entities.Combo {
	combo := entities.Combo{}
	err := smapping.FillStruct(&combo, smapping.MapFields(&comboDto))
	if err != nil {
		log.Fatal("Failed map %v ", err)
	}
	res := cb.comboRepository.Update(combo)
	return res
}

func (cb *comboService) UpdateStatus(comboDto dto.ComboUpdateDTO) {
	comboa := entities.Combo{}
	err := smapping.FillStruct(&comboa, smapping.MapFields(&comboDto))
	if err != nil {
		log.Fatal("Failed map %v ", err)
	}
	cb.comboRepository.UpdateStatus(comboa)
}