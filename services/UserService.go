package services

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/repositories"
	"log"

	"github.com/mashingan/smapping"
)

type UserService interface {
	All() []entities.User
	Me(Id uint64) entities.User
	Update(userDto dto.UpdateUserDTO) entities.User
	FindById(Id uint64) entities.User
	Delete(user entities.User)
}

type userService struct {
	userRepository repositories.UserRepository
}

func NewUserSerive(userRepository repositories.UserRepository) UserService {
	return &userService{
		userRepository: userRepository,
	}
}

func (service *userService) Delete(user entities.User) {
	service.userRepository.Delete(user)
}

func (service *userService) Update(usetDto dto.UpdateUserDTO) entities.User {
	user := entities.User{}

	err := smapping.FillStruct(&user, smapping.MapFields(&usetDto))
	if err != nil {
		log.Fatalf("Field map %v", err)
	}
	res := service.userRepository.Update(user)
	return res
}

func (service *userService) Me(Id uint64) entities.User {
	return service.userRepository.FindById(Id)
}

func (service *userService) FindById(Id uint64) entities.User {
	return service.userRepository.FindById(Id)
}

func (service *userService) All() []entities.User {
	return service.userRepository.All()
}
