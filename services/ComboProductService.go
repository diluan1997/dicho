package services

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/repositories"
	"log"

	"github.com/mashingan/smapping"
)

type ComboProductService interface {
	Create(comboProduct dto.ComboProductCreateDto) entities.ComboProduct
	Update(comboProductUpdate dto.ComboProductUpdateDto) entities.ComboProduct
	Delete(comboProduct entities.ComboProduct)
	FindById(ID uint64) entities.ComboProduct
	FindByProductAndComboID(ProductId uint64, ComboId uint64) entities.ComboProduct
}

type comboProductService struct {
	comboProductRepository repositories.ComboProductRepository
}

func NewComboProductService(comboProductRepository repositories.ComboProductRepository) ComboProductService {
	return &comboProductService{
		comboProductRepository: comboProductRepository,
	}
}

func (service *comboProductService) FindById(ID uint64) entities.ComboProduct {
	return service.comboProductRepository.FindById(ID)
}

func (service *comboProductService) FindByProductAndComboID(ProductId uint64, ComboId uint64) entities.ComboProduct {
	return service.comboProductRepository.FindByProductAndComboID(ProductId, ComboId)
}

func (service *comboProductService) Create(productDto dto.ComboProductCreateDto) entities.ComboProduct {
	product := entities.ComboProduct{}
	err := smapping.FillStruct(&product, smapping.MapFields(&productDto))
	if err != nil {
		log.Fatal("Failed Map Field %v ", err)
	}
	res := service.comboProductRepository.Create(product)
	return res
}

func (service *comboProductService) Update(comboProductUpdate dto.ComboProductUpdateDto) entities.ComboProduct {
	product := entities.ComboProduct{}
	err := smapping.FillStruct(&product, smapping.MapFields(&comboProductUpdate))
	if err != nil {
		log.Fatal("Failed Map Field %v ", err)
	}
	res := service.comboProductRepository.Update(product)
	return res
}

func (service *comboProductService) Delete(comboProduct entities.ComboProduct) {
	service.comboProductRepository.Delete(comboProduct)
}


