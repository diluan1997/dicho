package services

import (
	"fmt"
	"net/smtp"
)

type MailService interface {
	SendMail(email string, message string, token string) bool
}

type mailService struct {
	host      string
	port      string
	mail_from string
	password  string
}

func NewMailService() MailService {
	return &mailService{
		host:      "smtp.gmail.com",            //os.Getenv("MAIL_HOST"),
		port:      "587",                       // os.Getenv("MAIL_PORT"),
		mail_from: "duongdiluan1997@gmail.com", //os.Getenv("MAIL_FROM"),
		password:  "xwfvdojczkgheqbv",          // os.Getenv("MAIL_PASSWORD"),
	}
}

func (m *mailService) SendMail(email string, message string, token string) bool {
	auth := smtp.PlainAuth("", m.mail_from, m.password, m.host)
	fmt.Println(auth)

	fmt.Println(email + ":" + token)
	// url_verify := os.Getenv("BASE_URL") + "/api/v1/auth" + "/verify/" + token
	url_verify := "http://chungtoidicho.xyz" + "/v1/api/auth" + "/verify/" + token
	body := `
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<title>Hello, World</title>
			</head>
			<body>
				<p>` + message + `</p>
				<a href="` + url_verify + `"> Click vao day de kich hoat tai khoan</a>
			</body>
		</html>
		`
	subject := "Subject: " + message + "\n"
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	msg := []byte(subject + mime + body)
	err := smtp.SendMail(m.host+":"+m.port, auth, m.mail_from, []string{email}, []byte(msg))
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	return true
}
