package services

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/repositories"
	"log"

	"github.com/mashingan/smapping"
)

type OrderLineItemService interface{
	Create(orderLineItem dto.OrderLineItemCreateDTO) entities.OrderLineItem
}

type orderLineItemService struct{
	orderLineItemRepository repositories.OrderLineItemRepository
}

func NewOrderLineItemService(orderLineItemRepository repositories.OrderLineItemRepository) OrderLineItemService {
	return &orderLineItemService{
		orderLineItemRepository: orderLineItemRepository,
	}
}

func (service *orderLineItemService) Create(orderLineItem dto.OrderLineItemCreateDTO) entities.OrderLineItem {
	order_line_item := entities.OrderLineItem{}
	err := smapping.FillStruct(&order_line_item , smapping.MapFields(&orderLineItem))
	if err != nil {
		log.Fatalf("Failed map field %v" , err)
	}
	result := service.orderLineItemRepository.Create(order_line_item)
	return result
}