package services

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/repositories"
	"log"

	"github.com/mashingan/smapping"
)

type OrderService interface {
	All() []entities.Order
	Create(order dto.OrderCreateDTO) entities.Order
	CountOrderToDay(id uint64) uint64
	FindById(id uint64) entities.Order
	Delete(order entities.Order)
}

type orderService struct {
	orderRepository repositories.OrderRepository
}

func NewOrderService(orderRepository repositories.OrderRepository) OrderService {
	return &orderService{
		orderRepository: orderRepository,
	}
}

func (service *orderService) All() []entities.Order {
	return service.orderRepository.All()
}

func (service *orderService) FindById(id uint64) entities.Order {
	return service.orderRepository.FindById(id)
}

func (service *orderService) Create(orderDto dto.OrderCreateDTO) entities.Order {
	order := entities.Order{}
	err := smapping.FillStruct(&order, smapping.MapFields(&orderDto))
	if err != nil {
		log.Fatal("Failed  map field %v ", err)
	}
	res := service.orderRepository.Create(order)
	return res
}

func (service *orderService) Delete(order entities.Order) {
	service.orderRepository.Delete(order)
}

func (service *orderService) CountOrderToDay(id uint64) uint64 {
	return service.orderRepository.CountOrderToDay(id)
}
