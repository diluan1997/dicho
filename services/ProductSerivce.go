package services

import (
	"diluan/dto"
	"diluan/entities"
	"diluan/repositories"
	"log"

	"github.com/mashingan/smapping"
)

type ProductService interface {
	All(search string) []entities.Product
	Create(product dto.ProductCreateDTO) entities.Product
	Update(product dto.ProductUpdateDTO) entities.Product
	UpdateStatus(product dto.ProductUpdateDTO)
	FindByName(name string) bool
	CheckById(id uint64) bool
	FindById(id uint64) entities.Product
	FindByIds(IDs []string) []entities.Product
	FindNameDiffId(id uint64, name string) bool
	Delete(product entities.Product)
}

type productService struct {
	productRepository repositories.ProductRepository
}

func NewProductService(productRepository repositories.ProductRepository) ProductService {
	return &productService{
		productRepository: productRepository,
	}
}

func (service *productService) All(search string) []entities.Product {
	return service.productRepository.All(search)
}

func (cb *productService) FindByIds(IDs []string) []entities.Product {
	return cb.productRepository.FindByIds(IDs)
}

func (service *productService) Create(productDto dto.ProductCreateDTO) entities.Product {
	product := entities.Product{}
	err := smapping.FillStruct(&product, smapping.MapFields(&productDto))
	if err != nil {
		log.Fatal("Failed map field %v ", err.Error())
	}
	return service.productRepository.Create(product)

}

func (service *productService) Update(productDto dto.ProductUpdateDTO) entities.Product {
	product := entities.Product{}
	err := smapping.FillStruct(&product, smapping.MapFields(&productDto))
	if err != nil {
		log.Fatal("Failed map field %v ", err.Error())
	}
	res := service.productRepository.Update(product)
	return res
}

func (service *productService) UpdateStatus(productDto dto.ProductUpdateDTO) {
	product := entities.Product{}
	err := smapping.FillStruct(&product, smapping.MapFields(&productDto))
	if err != nil {
		log.Fatal("Failed map field %v ", err.Error())
	}
	service.productRepository.UpdateStatus(product)
}

func (service *productService) FindByName(name string) bool {
	return service.productRepository.FindByName(name)
}

func (service *productService) CheckById(id uint64) bool {
	return service.productRepository.CheckById(id)
}

func (service *productService) FindById(id uint64) entities.Product {
	return service.productRepository.FindById(id)
}

func (service *productService) FindNameDiffId(id uint64, name string) bool {
	return service.productRepository.FindNameDiffId(id, name)
}

func (service *productService) Delete(product entities.Product) {
	service.productRepository.Delete(product)
}
