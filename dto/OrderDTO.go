package dto

type OrderCreateDTO struct {
	Token      string `'json:"token"`
	UserId     uint64 `json:"user_id"`
	Note       string `json:"note" form:"note"`
	Status     string `json:"status"`
	ComboIds   string `json:"combo_ids" form:"combo_ids" validate:"required"`
	TotalPrice uint64 `json:"total_price"`
}
