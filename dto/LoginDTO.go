package dto

type LoginDTO struct {
	Email    string `json:"email" form:"email" binding:"required" validate:"email"`
	Password string `json:"password" form:"password" binding:"required" validate:"min6"`
	// Phone string `json:"phone" form:"phone" binding:"required" validate:"min6"`
}
