package dto

type OrderLineItemCreateDTO struct {
	Name        string `json:"name" form:"name" binding:"required"`
	Description string `json:"description" form:"description" binding:"required"`
	TotalPrice  uint64 `json:"total_price"`
	ImageUrl    string `json:"image_url"`
	ComboID     uint64 `json:"combo_id"`
	OrderID     uint64 `json:"order_id"`
}
