package dto

type ComboProductCreateDto struct {
	ComboID   uint64 `json:"combo_id" form:"combo_id" binding:"required"`
	ProductID uint64 `json:"product_id" form:"product_id" binding:"required"`
	Name      string `json:"name" form:"name" `
	Price     uint64 `json:"price" form:"price" `
	Quantity  uint64 `json:"quantity" form:"quantity" binding:"required"`
	Unit      string `json:"unit" form:"unit" `
	Status    bool   `json:"status" form:"status" `
	ImageUrl  string `json:"image_url" form:"image_url" `
}

type ComboProductUpdateDto struct {
	ID        uint64 `json:"id" form:"id" binding:"required"`
	// ComboID   uint64 `json:"combo_id" form:"combo_id" binding:"required"`
	// ProductID uint64 `json:"product_id" form:"product_id" binding:"required"`
	Quantity  uint64 `json:"quantity" form:"quantity" binding:"required"`
}
