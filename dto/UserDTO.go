package dto

import "time"

type VerifyAccountDTO struct {
	Token     string    `json:"token"`
	UpdatedAt time.Time `json:"updated_at"`
	Status    bool      `json:"status"`
}

type UpdateUserDTO struct {
	ID        uint64    `json:"id" from:"id"  validate:"required`
	Name      string    `json:"name" from:"name" `
	Phone     string    `json:"phone" from:"phone" `
	UpdatedAt time.Time `json:"updated_at"`
}
