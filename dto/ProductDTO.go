package dto

type ProductCreateDTO struct {
	Name        string `json:"name" form:"name" binding:"required"`
	Description string `json:"description" form:"description" binding:"required"`
	Price       uint64 `json:"price" form:"price" binding:"required`
	Status      bool   `json:"status" form:"status"`
	ImageUrl    string `json:"image_url"`
	Quantity    uint64 `json:"quantity" form:"quantity" binding:"required"`
	Unit        string `json:"unit" form:"unit" binding:"required"`
	CreatedBy   uint64 `json:"created_by"`
	UpdatedBy   uint64 `json:"updated_by"`
}

type ProductUpdateDTO struct {
	ID          uint64 `json:"id" form:"id" binding:"required"`
	Name        string `json:"name" form:"name"`
	Description string `json:"description" form:"description"`
	Price       uint64 `json:"price" form:"price"`
	Status      bool  `json:"status" form:"status"`
	Quantity    uint64 `json:"quantity" form:"quantity" `
	Unit        string `json:"unit" form:"unit" `
	ImageUrl    string `json:"image_url"`
	CreatedBy   uint64 `json:"created_by"`
	UpdatedBy   uint64 `json:"updated_by"`
}
