package dto

type Product struct {
	ID       uint64 `json:"id" form:"id"`
	Quantity uint64 `json:"quantity" form:"quantity"`
}

type ComboCreateDTO struct {
	Name        string `json:"name" form:"name" binding:"required"`
	LineItems   string `json:"line_items" form:"line_items" binding:"required"`
	Description string `json:"description" form:"description" binding:"required"`
	TotalPrice  uint64 `json:"total_price"`
	Status      bool   `json:"status" form:"status"`
	ImageUrl    string `json:"image_url"`
	CreatedBy   uint64 `json:"created_by"`
	UpdatedBy   uint64 `json:"updated_by"`
}

type ComboUpdateDTO struct {
	ID          uint64 `json:"id" form:"id" binding:"required"`
	Name        string `json:"name" form:"name"`
	// LineItems   string `json:"line_items" form:"line_items"`
	Description string `json:"description" form:"description"`
	TotalPrice  uint64 `json:"total_price"`
	Status      bool   `json:"status" form:"status"`
	ImageUrl    string `json:"image_url"`
	UpdatedBy   uint64 `json:"updated_by"`
}
